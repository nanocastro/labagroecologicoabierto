# # Laboratorio Agroecologico Abierto

Contacto: Fernando Castro (ferhcastro@gmail.com)

El laboratorio agroecológico abierto es una propuesta surgida a partir del diálogo entre la Cooperativa de Trabajo Ayllu, el CEFIC (Centro de Formación e Investigación Campesina) y lxs participantes de la red ReGOSH. 

La propuesta busca integrar los desarrollos de instrumental abierto de bajo costo en un laboratorio que acompañe a campesinos, agricultores, estudiantes e investigadores facilitando el seguimiento, la visualización y la sistematización de los cambios producidos en las fincas, particularmente en suelos, durante su transición hacia la agroecología.  

## Antecedentes y motivación

> Desde hace 30 años, el agronegocio viene envenenando nuestras mesas y arrasando en el camino con nuestra salud, el monte nativo, con indígenas y campesinos. Hoy es evidente la necesidad de transformar las formas en que nos relacionamos con la tierra, y entre nosotrxs, para producir nuestros alimentos. 

> El alimento saludable solo puede provenir de un suelo y agricultores regenerados. Desde la Cooperativa Ayllu y el CEFIC creemos en la necesidad de construir una agro-eco-cultura local a partir de apropiar los saberes de la agricultura orgánica, biodinámica, regenerativa e indígena. Necesitamos tomar sus prácticas y su ciencia, adaptarlas a nuestra región, a nuestros cultivos, necesitamos experimentar y documentar, comparar, sistematizar, traducir y compartir estas lecciones aprendidas para facilitar la transición agroecológica en otras fincas.


Jornada en la Escuela Campesina de Agroecología de Jocolí (UST)

> Durante este proceso de recuperación hacia la agroecología hemos encontrado toneladas de información acerca de los posibles caminos, prácticas y formas de abordar y diagnosticar la desintoxicación de la tierra. Sin embargo, nos resulta difícil encontrar datos y experiencias que hayan sistematizado los aprendizajes sobre la transición agroecológica en agroecosistemas similares a los nuestros; no hemos encontrado experiencia construida y estamos haciendo esta transición bastante a ciegas.

> Los laboratorios cercanos son pocos, los análisis costosos. y se encuentran aún en el paradigma físico químico convencional. En este paradigma se considera el suelo como una matriz inerte cuya fertilidad se define cuantificando la presencia de nutrientes (NPK). En el paradigma agroecológico el suelo es un sistema vivo y por lo tanto su salud es evaluada en términos biológicos (diversidad y actividad microbiológica, materia orgánica).

## Tecnologías abiertas para la transición agroecológica

> Desde hace algunos años junto a lxs integrantes del nodo ReGOSH, y particularmente con los Nodos de Mendoza y Chile, estamos experimentando con instrumental de laboratorio abierto/libre y métodos analíticos de bajo costo. Creemos que las tecnologías abiertas pueden ser instrumentales para acompañar procesos de transición agroecológica en sus dimensiones prácticas y pedagógicas, tanto en la evaluación de suelos y bioinsumos, como para luego avanzar en el análisis de producciones primarias y/o manufacturas derivadas. 

> Las tecnologías abiertas presentan la oportunidad de co-crear los diseños de las herramientas necesarias para producir conocimiento, aprovechando la experiencia de otros proyectos en diversos lugares del mundo, recreando lo que funciona y modificando lo que no para volver al diseño útil en contexto. Consideramos que esta plasticidad y potencial de apropiación de las tecnologías abiertas es lo que necesitamos para sistematizar los aprendizajes de la transición agroecológica local. Para ello necesitamos generar instancias donde poner los diseños a prueba a  campo, en diálogo con los actores locales, para adaptar o desarrollar nuevos métodos e instrumentos que resulten útiles a lxs usuarixs y en cada contexto en particular. 

## Nuestro objetivo

Prototipar un laboratorio campesino abierto para desarrollar y poner a prueba tecnologías abiertas y de bajo costo que permitan co-producir conocimiento y sistematizar los aprendizajes de la transición agroecológica a partir del diálogo entre diversos actores, sus prácticas actuales, sus visiones de futuro y los desafíos concretos que presenta la transición; y documentar la experiencia para que sea útil a otres en la región.

> ### Etapa Inicial – Prototipado

> El proceso de prototipado requiere poder iterar los desarrollos preliminares en diálogo con una diversidad de actores, hasta lograr cierta estabilidad de prácticas, diseños, métodos e instrumentos tanto en términos de alcance como de replicabilidad y usabilidad.  

> El espacio concreto que proponemos para desarrollar esta etapa inicial (12 meses) es la finca de Barrancas de la Cooperativa Ayllu donde comenzamos a recuperar, hace 3 años, un parral de 2 hectáreas de uva criolla que utilizamos para hacer jugo.

> ### Segunda Etapa – Proliferación

> En esta etapa las pruebas de concepto prototipadas en la etapa anterior se materializan en el armado de kits fáciles de usar acompañados por documentación detallada sobre su alcance y utilización.  

> El desarrollo de talleres conjuntos de ensamblado permite familiarizar a las usuarias con los kits, iniciando un programa de seguimiento sistemático sobre su uso e intercambio de las experiencias de las usuarias a lo largo del proceso. En este último sentido se está explorando el potencial acoplamiento con los Sistemas Participativos de Garantías.

## Tecnologías propuestas

> El foco de esta primera etapa estará puesto en métodos que permitan evaluar parámetros físico-químicos básicos (ie pH, conductividad, textura) y pondremos mayor énfasis en la microbiología del suelo y bioinsumos. Contamos con la mayor parte de las herramientas y partes necesarias para poner a punto estos métodos. 

> **Herramientas básicas para análisis físicoquímicos** de bioinsumos y suelo
Basadas en el uso de sensores de bajo costo (pH, conductividad, oxígeno disuelto, temperatura, humedad, color, turbidez, DQO, nitratos, fosfatos), plataformas abiertas de prototipado electrónico (Arduino) y el uso de teléfonos celulares. Los prototipos estarán basados en el trabajo que ya venimos realizando junto al grupo Co-Sensores

> Más información:
> https://gitlab.com/cosensores/ph-metro

> https://gitlab.com/nanocastro/ColorLabFD

> **Análisis microbiológicos basados en microscopía digital de alta resolución y bajo costo**
La idea es orientar el desarrollo de prototipos con los que ya hemos trabajado (ver proyecto OpenFlexure y experiencias propias aqui) con Fernan Federici y Pablo Cremades, para obtener panorámicas de suelos y bioinsumos (al estilo soil landscapes de Winogradsky). 

> Contraste de fase: https://build.openflexure.org/openflexure-delta-stage/v1.0.0/LED_grid_illumination 

> BrewerMicro https://gitlab.com/nanocastro/microbrew

> **Análisis microbiológicos indirectos mediante cámaras de respiraciòn de suelo**
Continuaremos el trabajo comenzado durante la primer residencia tecnológica de la red GOSH en Latinoamérica. (Colaboración Fernando Castro, Anabela Laudecina, Leandro Mastrantonio y Pablo Cremades)

> https://tecnologias.libres.cc/pablocremades/camara-respiracion-suelo

> **CromatografÍas de suelo**
Consideramos que esta herramienta es central debido a su difusión por distintos territorios, a la experiencia y el conocimiento construido con ella. Contamos con experiencia de desarrollo e implementación dentro del equipo.

> https://forum.openhardware.science/t/day-4-soil-chromatography-workshop/1512

### Participantes

Consideramos fundamental poder interactuar no solo con los actores locales sino también continuar nuestra vinculación con otros actores de Latinoamérica y el mundo trabajando en tecnologías abiertas.

* Cooperativa de Trabajo Ayllu. Somos una pequeña cooperativa de Mendoza (Argentina) que trabaja una finca de 6 hectáreas (principalmente vid). Producimos vino y jugo de uva. Somos urbanitas en transición a territorios y temporalidades rurales.
* CEFIC. Centro de Educación, Formación e Investigación Campesina (UST-MNCI Somos Tierra). 
* ReGOSH. Es una red Latinoamericana financiada por el programa iberoamericano CYTED que forma parte del movimiento global por el hardware científico abierto.Una de las actividades de la red es el desarrollo de residencias tecnológicas colaborativas. Como parte del equipo fundador, la tercera residencia se realizará en Mendoza para abordar temas de monitoreo ambiental y agroecología.
* Laboratorio de tecnologìas libres de FCEN (UN Cuyo). Junto a Pablo Cremades  venimos trabajando en tecnologías libres desde 2013 y continuamos colaborando actualmente en el marco de un Proyecto de Prácticas Socio Educativas y otro de extensión universitaria.
* INTA Estación Experimental Luján. Actualmente la Coop Ayllú cuenta con un Convenio de Vinculación Tecnológica para hacer pruebas experimentales con bioinsumos y cultivos de cobertura. Trabajamos con Ivan Funes Pinter y Martín Uliarte
* Cátedra de Tecnología Ambiental y Laboratorio de tecnologías Abiertas de la Facultad Nacional de Agronomía de la Universidad Nacional de Cuyo. 
* FedericiLab. Con este grupo venimos trabajando en distintos prototipos y talleres. Tenemos estrecha relación y, a través de ellos, hemos conseguido la mayor parte del instrumental con el que hoy contamos para el laboratorio.
* Grupo CoSensores. Compartimos muchos intereses y mantenemos un diálogo fluido para colaborar en nuestros distintos desarrollos.
* RealFoodLab. Este laboratorio fue creado en 2018 en EEUU a través de la Real Food Campaign para generar una base de datos pública y confiable para conocer la calidad de los suelos y los alimentos. Este laboratorio utiliza instrumental abierto que trata de compensar calidad, con costos y reproducibilidad de los métodos. En el pasado hemos colaborado con cromatografías de suelo junto a Leandro Mastrantonio.
* Humus sapiens. Esta es una de las vertientes de la red global de artistas e investigadores llamada Hackteria, especializada en instrumental abierto para biohacking. 
