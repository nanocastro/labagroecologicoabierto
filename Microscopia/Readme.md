## Análisis microbiológicos basados en microscopía digital de alta resolución y bajo costo

### Que queremos experimentar?
* distintos setups ópticos, sus posibilidades y límites de observación
* escaneo de las muestras y composición de imagenes [(stiching)](https://openflexure.discourse.group/t/stitching-images/194) para obtener micropanorámicas de suelo

### Donde Estamos?
Actualmente estamos trabajando en la puesta a punto de 2 microscopios [Openflexure](https://openflexure.org/projects/microscope/) de campo claro motorizados:
* uno con la óptica de la RPi
* otro con objetivo 40X  

Estamos esperando las partes ópticas y la iluminación para construir un tercero de [tipo Delta](https://openflexure.org/projects/deltastage/) para un tipo de liminación que permita un [(pseudo) contraste de fase](https://build.openflexure.org/openflexure-delta-stage/v1.1.0/LED_grid_illumination.html).  
[Galeria de imágenes](https://www.flickr.com/people/193163840@N03/)

### Algunas referencias
* [DIY guide for microscopy of agricultural soils](https://www.mikroliv.no/Microscope%20guide%20-%20English.pdf)
* FAO. [A practical manual of soil microbiology laboratory methods](http://www.fao.org/3/aq359e/aq359e.pdf)
* Recuento automático para obtener relación F/B (fungal/bacteria) de una muestra
https://symsoil.com/low-cost-fb-ratio-analysis/
* SoilfoodWeb. Elaine Ingham video https://www.youtube.com/watch?v=5MFoQDdENS4 
* Living Web Farms. Meredith Leigh videos [Parte 1](https://www.youtube.com/watch?v=eG5eQroUSGo&t=829s). [Parte 2](https://www.youtube.com/watch?v=D_FGijGBZW4&t=1575s)

### Documentacion para replica local de microscopios
BOM (en proceso)







