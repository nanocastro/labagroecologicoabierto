$fn=100;
difference(){
union(){
cylinder(h=1.5,d=30,center=true);
    translate([0,-2.5,-0.75])
    cube([75,5,1.5]);
    translate([75,-2.5,-4.25])
    cube([1.5,5,5]);
    rotate([0,0,90])
    translate([0,-2.5,-0.75])
    cube([75,5,1.5]);
    rotate([0,0,90])
    translate([75,-2.5,-4.25])
    cube([1.5,5,5]);
}
cylinder(h=4,d1=2,d2=4,center=true);}

